import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home.vue'
import EditProfile from '../components/EditProfile.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/user/:id',  
      name: 'user',
      component: EditProfile,
    }
  ]
})