import Vue from 'vue'
import Vuex from 'vuex'
import WordService from '../services/dataService.js'

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
		users: [],
		selectedUser: null,
	},
	mutations: {
		SAVE_USERS(state, users) {
			state.users = users
		},
		SET_SELECTED_USER(state,user) {
			state.selectedUser = user
		},
		UPDATE_USER(state, user) {
			let userIndex = state.users.findIndex(u => u.id == user.id);
			state.users[userIndex] = user
		},
		DELETE_USER(state, user) {
			state.users = state.users.filter(u => u.id !== user.id);
		}
			
	},
	getters: {
		users: state => state.users,
		selectedUser: state => state.selectedUser
	},
	actions: {
		async loadUsers({commit}) {
			const response = await WordService.getData()
			commit('SAVE_USERS', response.data)
		},
		setSelectedUser({commit}, user) {
			commit('SET_SELECTED_USER', user)
		},
		updateUser({commit}, user) {
			commit('UPDATE_USER', user)
		},
		deleteUser({commit}, user) {
			commit('DELETE_USER', user)
		}

	},
})